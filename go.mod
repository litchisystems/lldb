module gitlab.com/litchisystems/lldb

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.3.4
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/pkg/errors v0.9.1
	gitlab.com/litchisystems/lldb/internal/v1/protocol v1.0.0
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	github.com/google/uuid v1.3.0
)

replace gitlab.com/litchisystems/lldb/internal/v1/protocol => ./internal/v1/protocol

# LLDB (LitchiLyrics database)

This connector is released as small executable or docker image and connects to an mqtt server to serve a song database.

## Usage

1. Copy the env.example to .env
2. Modify the content of your .env
3. Run `docker-compose up -d`


## Development setup

### Run a mqtt broker:

1. Install mosquitto (Windows: `winget install EclipseFoundation.Mosquitto` and activate service)
2. Optional: Install mqtt explorer (Windows: `winget install thomasnordquist.MQTT-Explorer`)
3. Create pwfile and aclfile in `/etc/mosquitto` or `C:\Program Files\Mosquitto` under Windows.
4. Run mosquitto: (Windows: `net start mosquitto`)
4. Allow port 1883 in firewall to connect from other devices in your network.

### Build and run lldb:

1. Install golang. (Windows: `winget install GoLang.Go` and add it to PATH)
2. On windows install gcc manually: https://jmeubank.github.io/tdm-gcc/
3. Download dependencies: `go mod download`
4. Build it with: `go build .` and run it by running `lldb` or `lldb.exe` on Windows.
6. Or run it directly: `go run .`


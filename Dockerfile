FROM golang:1.12 AS build

# Set the Current Working Directory inside the container
ENV HOME /app

# We want to populate the module cache based on the go.{mod,sum} files.
WORKDIR /app
COPY . .
RUN go mod download

# Unit tests
# RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN go build -o lldb .

# Start fresh from a smaller image
FROM debian:buster-slim

# Install ca-certificates
RUN apt-get update \
  && apt-get install -y --no-install-recommends ca-certificates \
  && rm -rf /var/lib/apt/lists/*

COPY --from=build /app/lldb /app/lldb

WORKDIR /data
VOLUME /data

# Run the binary program produced by `go install`
ENTRYPOINT ["/app/lldb"]

package protocol

import "encoding/xml"

// TOPICS:
//QOS_0 := byte(0) // maybe not delivered (1p), NOT queued by p. session
const qos1 = byte(1) // delivered at least once (2p), queued by p. session
//QOS_2 := byte(1) // delivered exactly once (4p), queued by p. session

// DB Health: (online, offline) [RETAINED]
const TopicHealth = "%sdb/v1/health"
const TopicHealthQos = qos1
const TopicHealthRetained = true

// Clients subscribes to db info to see if db was resetted:
const TopicDBInfo = "%sdb/v1/info"
const TopicDBInfoQos = qos1
const TopicDBInfoRetained = true

// Clients publish and subscribe patches (list of songs) to [username/version_id/uuid]:
const TopicPatchesLatestChanges = "%sdb/v1/patches/latest/changes/+/+/+"
const TopicPatchesLatestChangesQos = qos1
const TopicPatchesLatestChangesRetained = false

// Clients subscribe for approving (version, timestamp, username, uuid) to: [RETAINED]
const TopicPatchesLatestVersion = "%sdb/v1/patches/latest/version"
const TopicPatchesLatestVersionQos = qos1
const TopicPatchesLatestVersionRetained = true

// Clients subscribe for reject (uuid, reason) to:
const TopicPatchesLatestReject = "%sdb/v1/patches/latest/reject"
const TopicPatchesLatestRejectQos = qos1
const TopicPatchesLatestRejectRetained = false

// If client needs a older version, it may request it by publishing the id to:
const TopicPatchesRequestGet = "%sdb/v1/patches/request/+/get"
const TopicPatchesRequestGetQos = qos1
const TopicPatchesRequestGetRetained = false

// Otherwise it will receive the requested patches in one message at:
const TopicPatchesRequestPatches = "%sdb/v1/patches/request/+/patches"
const TopicPatchesRequestPatchesQos = qos1
const TopicPatchesRequestPatchesRetained = false

// Or the complete database at the last state [version]:
const TopicPatchesRequestFull = "%sdb/v1/patches/request/+/full/+"
const TopicPatchesRequestFullQos = qos1
const TopicPatchesRequestFullRetained = false

// Client ACL rules, to match multiple topics:
// ACL  S health/#
// ACL  S db/v1/patches/latest/#
//      S db/v1/patches/latest/patch/+/+
//      S db/v1/patches/latest/version [RETAINED]
//      S db/v1/patches/latest/reject
// ACL PS db/v1/patches/request/[CLIENT_ID]/+
//     P  db/v1/patches/request/[CLIENT_ID]/get
//      S db/v1/patches/request/[CLIENT_ID]/patch
//     P  db/v1/patches/request/[CLIENT_ID]/version
// Write:
// ACL P  db/v1/patches/latest/patch/#

// Client persistend session usage: (optional)
// This would reduce request packages, and allows an offline database for persistend read only clients.
//songpos: qos = 0 [retained]
//song: qos = 0 [retained]
//playlist: qos = 0 [retained]
//patches/latest/patch qos = 1, in p. session
//patches/latest/version qos = 1, in p. session, [no retain needed]
//patches/latest/reject qos = 1, in p. session
//patches/request/+/get qos = 1, for full request if no session found

type DBInfo struct {
	XMLName xml.Name `xml:"dbInfo"`
	UUID    string   `xml:"uuid"`
}

type ChangeReject struct {
	XMLName xml.Name `xml:"changeReject"`
	UUID    string   `xml:"uuid"`
	Reason  string   `xml:"reason"`
}

type Version struct {
	XMLName   xml.Name `xml:"version"`
	Id        uint64   `xml:"id"`
	Timestamp string   `xml:"ts"`
	Username  string   `xml:"user"`
	UUID      string   `xml:"uuid"`
	DBUUID    string   `xml:"dbuuid"`
}

type PatchRequest struct {
	XMLName       xml.Name `xml:"patchRequest"`
	ID            uint64   `xml:"id"`
	ToID          uint64   `xml:"toID"`
	AllowCombined bool     `xml:"allowCombined"`
	AllowFull     bool     `xml:"allowFull"`
}

type InnerXMLPatches struct {
	XMLName xml.Name        `xml:"patches"`
	Patches []InnerXMLPatch `xml:"patch"`
}

type InnerXMLPatch struct {
	XMLName xml.Name      `xml:"patch"`
	Version Version       `xml:"version"`
	Songs   InnerXMLSongs `xml:"songs"`
}

type InnerXMLSongs struct {
	XMLName  xml.Name `xml:"songs"`
	InnerXML string   `xml:",innerxml"`
}

type Songs struct {
	XMLName xml.Name `xml:"songs"`
	Song    []Song   `xml:"song"`
}

type Song struct {
	XMLName                  xml.Name `xml:"song"`
	Title                    string   `xml:"title"`
	Composer                 string   `xml:"composer"`
	AuthorText               string   `xml:"authorText"`
	AuthorTranslation        string   `xml:"authorTranslation"`
	Publisher                string   `xml:"publisher"`
	AdditionalCopyrightNotes string   `xml:"additionalCopyrightNotes"`
	Language                 string   `xml:"language"`
	SongNotes                string   `xml:"songNotes"`
	Tonality                 string   `xml:"tonality"`
	Uuid                     string   `xml:"uuid"`
	ChordSequence            string   `xml:"chordSequence"`
	DrumNotes                string   `xml:"drumNotes"`
	Tempo                    string   `xml:"tempo"`
	Lyrics                   string   `xml:"lyrics"`
}

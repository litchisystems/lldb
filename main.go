package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/joho/godotenv"

	// https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully
	errors "github.com/pkg/errors"

	"database/sql"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/litchisystems/lldb/internal/v1/protocol"
)

func dbCreate(db *sql.DB) {
	sqlStmt := `
	CREATE TABLE IF NOT EXISTS patches (id INTEGER PRIMARY KEY AUTOINCREMENT, ts DATETIME DEFAULT CURRENT_TIMESTAMP, username text, uuid text, patch text);
	CREATE TABLE IF NOT EXISTS db_info (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid text);
	`
	_, err := db.Exec(sqlStmt)
	if err != nil {
		log.Panic(err)
	}

	dbuuid, err := dbGetDBInfo(db)
	if dbuuid == "" {
		dbuuid := uuid.New().String()
		log.Printf("DB UUID [%s]", dbuuid)
		_, err = db.Exec("insert into db_info(uuid) values(?);", dbuuid)
		if err != nil {
			log.Panic(err)
		}
	}
}

// ErrOldVersion if patch is too old
var ErrOldVersion = errors.New("db: mismatching patch version")

// ErrDublicatePatch if patch equals with last patch
var ErrDublicatePatch = errors.New("db: patch equals last patch")

func dbAddPatch(db *sql.DB, username string, newID uint64, uuid string, patch string) (uint64, time.Time, error) {
	var id uint64
	var lastUUID string
	var dberr error
	var ts time.Time
	err := db.QueryRow("select id, uuid from patches order by id desc limit 1;").Scan(&id, &lastUUID)
	if err != nil {
		//TODO: first row:
		id = 0
		lastUUID = "NOUUID"
		//log.Panic(err)
	}
	if id+1 != newID {
		dberr = ErrOldVersion
	} else if uuid == lastUUID {
		dberr = ErrDublicatePatch
	} else {
		_, err = db.Exec("insert into patches(username, uuid, patch) values(?, ?, ?);", username, uuid, patch)
		if err != nil {
			log.Fatal(err)
		}
		err = db.QueryRow("select id, ts from patches order by id desc limit 1;").Scan(&id, &ts)
		if err != nil {
			log.Panic(err)
		}
	}
	return id, ts, dberr
}

func dbLatestPatch(db *sql.DB) (uint64, time.Time, string, string, error) {
	var id uint64
	var ts time.Time
	var username string
	var uuid string
	err := db.QueryRow("select id, ts, username, uuid from patches order by id desc limit 1;").Scan(&id, &ts, &username, &uuid)
	if err != nil {
		// log.Panic(err)
	}
	return id, ts, username, uuid, err
}

func dbGetPatch(db *sql.DB, id uint64) (uint64, time.Time, string, string, string, error) {
	var uuid string
	var username string
	//var dberr error
	var ts time.Time
	var patch string
	err := db.QueryRow("select id, ts, uuid, username, patch from patches where id == ?;", id).Scan(&id, &ts, &uuid, &username, &patch)
	if err != nil {
		// log.Panic(err)
	}
	return id, ts, uuid, username, patch, err
}

func dbGetPatches(db *sql.DB, id uint64, toId uint64) (*sql.Rows, error) {
	limit := ""
	if toId > 0 {
		limit = fmt.Sprintf(" limit %d", toId+1-id)
	}
	rows, err := db.Query("select id, ts, uuid, username, patch from patches where id >= ? order by id asc"+limit+";", id)
	return rows, errors.Wrap(err, "Could not read multiple patches")
}

func dbGetDBInfo(db *sql.DB) (string, error) {
	var uuid string
	err := db.QueryRow("select uuid from db_info order by id desc limit 1;").Scan(&uuid)
	if err != nil {
		log.Print(err)
	}
	return uuid, err
}

func pub(client mqtt.Client, topic string, qos byte, retained bool, message interface{}) {
	log.Printf("P [%s] (q%d r%t)", topic, qos, retained)
	// log.Printf("P [%s] (q%d r%t) : %.50s...", topic, qos, retained, strings.ReplaceAll(message, "\n", "\\n"))
	if token := client.Publish(topic, qos, retained, message); token.Wait() && token.Error() != nil {
		log.Panic(token.Error())
	}
}

func sub(client mqtt.Client, topic string, qos byte, callback mqtt.MessageHandler) {
	log.Printf("S [%s] (q%d)", topic, qos)
	if token := client.Subscribe(topic, qos, func(client mqtt.Client, msg mqtt.Message) {
		log.Printf("S [%s] (q%d r%t) : %.50s...", msg.Topic(), msg.Qos(), msg.Retained(), strings.ReplaceAll(string(msg.Payload()), "\n", "\\n"))
		callback(client, msg)
	}); token.Wait() && token.Error() != nil {
		log.Panic(token.Error())
	}
}

func latestPatchMessageHandler(db *sql.DB, client mqtt.Client, mqttPrefix string, msg mqtt.Message, dbuuid string) {
	topicPatchesLatestReject := fmt.Sprintf(protocol.TopicPatchesLatestReject, mqttPrefix)
	topicPatchesLatestVersion := fmt.Sprintf(protocol.TopicPatchesLatestVersion, mqttPrefix)

	topicParts := strings.Split(msg.Topic(), "/")
	username := topicParts[len(topicParts)-3]
	id, err := strconv.ParseUint(topicParts[len(topicParts)-2], 10, 64)
	uuid := topicParts[len(topicParts)-1]
	if err == nil {
		var ts time.Time
		id, ts, err = dbAddPatch(db, username, id, uuid, string(msg.Payload()))
		if err == nil {
			err = publishPatchInfo(client,
				topicPatchesLatestVersion,
				protocol.TopicPatchesLatestVersionQos,
				protocol.TopicPatchesLatestVersionRetained, id, username, ts, uuid, dbuuid)
			if err != nil {
				log.Panic(errors.Wrap(err, "Patch added to db, but could not publish this change!"))
			}
		}
	}
	if err != nil {
		log.Print(errors.Wrap(err, "Could not apply patch"))
		changeReject, err := xml.Marshal(protocol.ChangeReject{
			UUID:   uuid,
			Reason: err.Error(),
		})
		if err != nil {
			log.Print(errors.Wrap(err, "Could not marshal reject"))
			return
		}
		pub(client, topicPatchesLatestReject, protocol.TopicPatchesLatestRejectQos, protocol.TopicPatchesLatestRejectRetained, changeReject)
	}
}

func publishPatchInfo(client mqtt.Client, topic string, qos byte, retained bool, id uint64, username string, ts time.Time, uuid string, dbuuid string) error {
	patchInfoMessage, err := xml.Marshal(protocol.Version{
		Id:        id,
		Username:  username,
		Timestamp: ts.Format(time.RFC3339),
		UUID:      uuid,
		DBUUID:    dbuuid,
	})
	if err != nil {
		return errors.Wrap(err, "Publish patch info failed")
	}
	pub(client, topic, qos, retained, patchInfoMessage)
	return nil
}

// To combine a range of patches to a single mqtt message and send it.
// Use 0 as toVersion, to send all following from version.
func publishPatchesToClient(db *sql.DB, client mqtt.Client, mqttPrefix string, version uint64, toVersion uint64, clientid string, dbUUID string) error {
	topicPatchesRequestPatches := fmt.Sprintf(protocol.TopicPatchesRequestPatches, mqttPrefix)
	topicPatchesRequestPatches = strings.Replace(topicPatchesRequestPatches, "+", clientid, 1)

	rows, err := dbGetPatches(db, version, toVersion)
	defer rows.Close()
	if err != nil {
		return errors.Wrap(err, "Could not publish multiple patches")
	}
	patches := protocol.InnerXMLPatches{}
	for rows.Next() {
		patch := protocol.InnerXMLPatch{}
		patch.Version.DBUUID = dbUUID
		var songsXml []byte
		rows.Scan(&patch.Version.Id, &patch.Version.Timestamp, &patch.Version.UUID, &patch.Version.Username, &songsXml)
		if err := xml.Unmarshal(songsXml, &patch.Songs); err != nil {
			return errors.Wrap(err, "Could not unmarshal old patch")
		}
		patches.Patches = append(patches.Patches, patch)
	}
	patch, err := xml.Marshal(patches)
	if err != nil {
		return errors.Wrap(err, "Could not marshal combined patch")
	}
	pub(client, topicPatchesRequestPatches, protocol.TopicPatchesRequestPatchesQos, protocol.TopicPatchesRequestPatchesRetained, patch)
	return nil
}

func patchesRequestGetMessageHandler(db *sql.DB, client mqtt.Client, mqttPrefix string, msg mqtt.Message, dbUUID string) {
	topicParts := strings.Split(msg.Topic(), "/")
	clientid := topicParts[len(topicParts)-2]
	patchRequest := protocol.PatchRequest{
		ToID: 0,
	}
	if err := xml.Unmarshal(msg.Payload(), &patchRequest); err != nil {
		log.Print(errors.Wrap(err, "patchesRequestGet failed to parse request"))
		return
	}
	err := publishPatchesToClient(db, client, mqttPrefix, patchRequest.ID, patchRequest.ToID, clientid, dbUUID)
	if err != nil {
		log.Print(errors.Wrap(err, "patchesRequestGet failed to publish to client"))
		return
	}
}

func publishDBInfo(client mqtt.Client, topic string, qos byte, retained bool, uuid string) error {
	dbInfoMessage, err := xml.Marshal(protocol.DBInfo{
		UUID: uuid,
	})
	if err != nil {
		return errors.Wrap(err, "Publish db info failed")
	}
	pub(client, topic, qos, retained, dbInfoMessage)
	return nil
}

func main() {
	//Load dotenv:
	if _, err := os.Stat(".env"); err == nil {
		err = godotenv.Load(".env")
		if err != nil {
			log.Panic(errors.Wrap(err, "Error loading .env file"))
		}
	}
	//Load and check environment
	dbFile := os.Getenv("DB_FILE")
	mqttBroker := os.Getenv("MQTT_BROKER")
	mqttUsername := os.Getenv("MQTT_USERNAME")
	mqttPassword := os.Getenv("MQTT_PASSWORD")
	mqttPrefix := os.Getenv("MQTT_PREFIX")
	// Unique persistend clientid. At least 23 chars:
	mqttClientID := os.Getenv("MQTT_CLIENT_ID")
	// KeepAlive. Possible up to 18hours
	mqttKeepAlive := os.Getenv("MQTT_KEEP_ALIVE")
	if dbFile == "" {
		panic("required env var DB_FILE not set")
	}
	if mqttBroker == "" {
		panic("required env var MQTT_BROKER not set")
	}
	if mqttUsername != "" && mqttPassword == "" {
		panic("required env var MQTT_PASSWORD not set")
	}
	if mqttClientID == "" {
		panic("required env var MQTT_CLIENT_ID not set")
	}
	if mqttPrefix != "" {
		mqttPrefix += "/"
	}
	if mqttKeepAlive == "" {
		mqttKeepAlive = "60"
	}
	mqttKeepAliveDuration, err := time.ParseDuration(mqttKeepAlive)
	if err != nil {
		log.Panic(err)
	}

	// Topics:
	topicHealth := fmt.Sprintf(protocol.TopicHealth, mqttPrefix)
	topicDBInfo := fmt.Sprintf(protocol.TopicDBInfo, mqttPrefix)
	topicPatchesLatestPatch := fmt.Sprintf(protocol.TopicPatchesLatestChanges, mqttPrefix)
	topicPatchesLatestVersion := fmt.Sprintf(protocol.TopicPatchesLatestVersion, mqttPrefix)
	topicPatchesRequestGet := fmt.Sprintf(protocol.TopicPatchesRequestGet, mqttPrefix)

	// Create DB
	log.Printf("Opening db: %s", dbFile)
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Panic(err)
	}
	defer db.Close()
	dbCreate(db)
	dbuuid, err := dbGetDBInfo(db)

	// Connection config
	opts := mqtt.NewClientOptions()
	opts.AddBroker(mqttBroker)
	if mqttUsername != "" {
		opts.SetUsername(mqttUsername)
		opts.SetPassword(mqttPassword)
	}
	opts.SetWill(fmt.Sprintf(protocol.TopicHealth, mqttPrefix), "offline", protocol.TopicHealthQos, protocol.TopicHealthRetained)
	opts.SetKeepAlive(mqttKeepAliveDuration)
	opts.SetClientID(mqttClientID)
	opts.SetOrderMatters(true) // Subscriptions come in ordered
	// opts.SetAutoReconnect(true)
	// opts.SetMaxReconnectInterval(10 * time.Second)
	opts.SetConnectionLostHandler(func(client mqtt.Client, reason error) {
		log.Panicf("Connection lost: %s", reason.Error())
		// log.Print(errors.Wrap(reason, "Connection lost"))
	})
	// opts.SetReconnectingHandler(func(client mqtt.Client, opts *mqtt.ClientOptions) {
	// 	log.Print("Reconnecting...")
	// })
	// Connect
	log.Printf("Connecting to broker %s", mqttBroker)
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Panic(token.Error())
	}

	// Wait for wg.Done()
	var wg sync.WaitGroup
	wg.Add(1)
	// Run the following maybe inside a subscription to exit gracefully.
	// wg.Done()

	// Subs:
	sub(client, topicPatchesLatestPatch, protocol.TopicPatchesLatestChangesQos, func(client mqtt.Client, msg mqtt.Message) {
		go latestPatchMessageHandler(db, client, mqttPrefix, msg, dbuuid)
	})
	sub(client, topicPatchesRequestGet, protocol.TopicPatchesRequestGetQos, func(client mqtt.Client, msg mqtt.Message) {
		go patchesRequestGetMessageHandler(db, client, mqttPrefix, msg, dbuuid)
	})

	// Pubs:
	publishDBInfo(client, topicDBInfo, protocol.TopicDBInfoQos, protocol.TopicDBInfoRetained, dbuuid)
	id, ts, username, uuid, err := dbLatestPatch(db)
	if err == nil {
		err = publishPatchInfo(client, topicPatchesLatestVersion, protocol.TopicPatchesLatestVersionQos, protocol.TopicPatchesLatestVersionRetained, id, username, ts, uuid, dbuuid)
	} else {
		err = publishPatchInfo(client, topicPatchesLatestVersion, protocol.TopicPatchesLatestVersionQos, protocol.TopicPatchesLatestVersionRetained, 0, "dbreset", time.Now(), "nouuid", dbuuid)
	}
	if err != nil {
		log.Panic(errors.Wrap(err, "Could not publish initial latest patch!"))
	}
	pub(client, topicHealth, protocol.TopicHealthQos, protocol.TopicHealthRetained, "online")

	log.Printf("Ready & waiting for requests!")
	wg.Wait()
}
